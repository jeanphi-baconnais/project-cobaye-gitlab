[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/jeanphi.baconnais/project-cobaye-gitlab)

# 🦊 GitLab Project Test

This is a public project to test GitLab functionalities 🦊


## Quarkus Note

This project uses Quarkus, the Supersonic Subatomic Java Framework. If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

### Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.


## Program of this project 

- ✅ Dev with using Gitpod 
- ✅ Save a new Quarkus project with a single simple REST resource
- ✅ GitLab CI : Test the application 
- ✅ GitLab CI : Slack integration 
- ✅ GitLab CI : Add first rules to commit only when ```backend``` directory is changed
- ✅ GitLab CI : Build project
- ✅ Save an image in the registry
- ✅ GitLab CI : Verify test (in MR)
- 💥 GitLab CI : Code coverage. 
     - i don't try to search a sonar image for arm. 
     - add template code quality but not ok in arm
- ✅ GitLab CI : Install Runner on a personal system (raspberry PI)
     - blog post in progress
- ✅ GitLab CI : test !reference
- ✅ GitLab CI : cache management
- GitLab CI : deploy on GKE
- GitLab CI : use services
- GitLab CI : OWASP ?


>  **Legend** <br /> 
✅ : Task done - 
🧭 : Task in progress - 
⏸ : Task in pause - 
➡️ : Next task - 
❌ : Task in error
💥 : task laissé de côté
